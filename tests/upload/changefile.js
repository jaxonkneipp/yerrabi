function getFileType(filename) {
  return filename.split('.').pop();
}
