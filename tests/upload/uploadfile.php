<?php
    $trail_name = $_POST["trail_name"];
    // collects timestamp, day,hours,munites,seconds
    $unique = preg_replace('/\s+/', '', $trail_name) . date("zHis");

    //DEBUG (4):
        // echo $unique;
        // print_r($_POST);
        // print_r($_FILES);
        // echo $trail_name;

    // Temp dir for uploaded file
    $target_dir = "temp";
    $basename = basename($_FILES['file']['name']);
    $target_file = "$target_dir/$basename";

    // gets the filetype
    $file_type = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

    //check whether filetype is valid, can only accept gpx, kml or geojson files.
    if ($file_type != "geojson" && $file_type != "gpx" && $file_type != "kml") {
      echo "Invalid filetype uploaded!";
      die; // Don't run rest of script
    }

    $target_file = "$target_dir/$unique.$file_type";
    
    // DEBUG (3):
        // echo $unique;
        // echo $target_file;
        //echo "<br>" . $target_file . "<br>";

    // Moves file to the temp dir
    move_uploaded_file($_FILES['file']['tmp_name'], $target_file);
    // If the file type is a .gpx or .kml then convert
    if ($file_type != "geojson") {
        $script = "./convertfile.sh '$target_file' '$unique'";

        //DEBUG (1):
            echo $script . "<br>";

        echo shell_exec($script);

        // move the file to the data folder
        append_to_json($trail_name, "usr/$unique.geojson");
        echo rename("temp/$unique.geojson", "../../data/usr/$unique.geojson");

    } else { // just append and move
      //DEBUG (1):
          //echo "<br> geojson <br>";

      //appends to the data.json
      append_to_json($trail_name, "usr/$unique.geojson");
      // Actually moves the file. not renames, should return true
      echo rename("temp/$unique.geojson", "../../data/usr/$unique.geojson");
        //move_uploaded_file($target_file, "usr/");
    }
    // TODO: Change file location here.
    header("location: ../maps/main.html");

    // Append to the data.json file
    function append_to_json ($name, $url) {
        // TODO: Change file strucure here, when website is moved
        $file_path = "../../data/data.json";

        // Read the file
        $dataJSON = file_get_contents($file_path);

        // Decode into array format
        $data = json_decode($dataJSON, true);

        // DEBUG (3):
            // echo "<pre>";
            // print_r($data);
            // echo "</pre>";

        // Insert into the last spot of trail array
        $data["trail"][] = array(
            'name' => $name,
            'url'  => $url,
            'type' => 'usr'
          );


        // DEBUG (3):
                // echo "<pre>";
                // print_r($data);
                // echo "</pre>";

        // Encode data then rewrite file data.json
        $data = json_encode($data, true);
        file_put_contents($file_path, $data);
    }
?>
